const fs = require('fs');

module.exports = {
    init() {
        return null;
    },
    getMessages(messages) {
        return messages;
    },
    addMessage(message) {
        const date = new Date().toISOString();
        const filename = `./messages/${date}.txt`;
        const messages = {
            message: message,
            dateTime: date,
        };
        fs.writeFileSync(filename, JSON.stringify(messages, null, 2))
    },
};