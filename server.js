const express = require('express');
const messages = require('./app/messages');
const getMessages = require('./app/getMessages');
const db = require('./db/fileDB');

db.init();

const app = express();
app.use(express.json());

const port = 8000;

app.use('/messages', messages);
app.use('/messages', getMessages);

app.listen(port, () => {
    console.log('Наш порт: ' + port)
});
