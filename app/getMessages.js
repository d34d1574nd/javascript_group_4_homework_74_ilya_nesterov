const express = require('express');
const fs = require('fs');
const db = require('../db/fileDB');

const path = './messages';

const router = express.Router();

router.get('/', (req, res) => {
    fs.readdir(path, (err, files) => {
        let messages = [];
        files.forEach(file => {
            const messageRead = fs.readFileSync(path + '/' + file);
            const message = JSON.parse(messageRead);
            messages.push({message: message.message});
        });
        res.send(db.getMessages(messages.slice(-5)));
    });
});

module.exports = router;