const express = require('express');
const db = require('../db/fileDB');

const router = express.Router();

router.post('/', (req, res) => {
    db.addMessage(req.body.message);
    res.send('OK');
});

module.exports = router;

